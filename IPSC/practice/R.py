import linecache

def getNumberOfExplosives(n):
    sum = 0
    for i in xrange(n):
        for j in xrange(n):
            for k in xrange(n):
                t = (i **2 + j ** 2 + k ** 2)** 0.5
                if t - int(t) == 0:
                    sum += 1
    return sum * 2 - 1

def main():
    fname = raw_input("Enter the path to the file: ")
    ansfile = open("../data/R.ans.txt", "w")
    c = 3
    while True:
        l = linecache.getline(fname, c).strip()
        if not l: break
        num = getNumberOfExplosives(int(l))
        ansfile.write("{0}\n".format(num))
        print num
        c += 2
    ansfile.close()
    print "answer successfully written to ../data/R.ans.txt"

main()
