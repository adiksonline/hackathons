# -*- coding: utf8 -*-

class Dancing(object):
    def __init__(self, suprise, minBest, scoresList):
        self.suprise = suprise
        self.minBest = minBest
        self.scoresList = scoresList

    def countSuprise(self):
        count = 0
        for i in self.triplets:
            i.sort()
            if i[2] - i[0] > 1:
                count += 1
        return count

    def tripletize(self, score):
        temp = score/3
        triplet = [temp, temp, temp]
        rem = score - temp * 3
        if rem == 1:
            triplet[2] += 1
        elif rem == 2:
            triplet[1] +=  1
            triplet[2] += 1
        return triplet

    def tripletAll(self):
        self.triplets = []
        for i in self.scoresList:
            self.triplets.append(self.tripletize(i))
        if self.countSuprise() < self.suprise:
            self.reTrip()

    def reTrip(self):
        for i in self.triplets:
            i.sort()
            if (i[1] == i[2]) and (i[1] > i[0]) and (i[1] - i[0] <= 1):# and ((self.minBest - i[2]) >= 0):
                i[1] -= 1
                i[2] += 1
            #if self.countSuprise() == self.suprise:
                #return
        for i in self.triplets:
            i.sort()
            if i[2] - i[0] == 0 and i[0] != 0:
                i[0] -= 1
                i[2] += 1
            if self.countSuprise() == self.suprise:
                return
        for i in self.triplets:
            if i[2] >  i[0] and i[2] >  self.minBest and self.countSuprise() >  self.suprise:
                i[0] +=  1
                i[2] -=  1
            if i[2] >  i[0] and i[2] < self.minBest and self.countSuprise() >  self.suprise:
                i[0] +=  1
                i[2] -=  1

    def hasBestScore(self, cur):
        cur.sort()
        if cur[2] >= self.minBest:
            return True

    def bests(self):
        count = 0
        for i in self.triplets:
            if self.hasBestScore(i):
                count += 1
        return count


def main():
    print "Ensure you are in the question folder and the folder is writable"
    que = raw_input("enter the filename of the question: ")
    q = open(que, "r")
    q.readline() #omit the first line
    result = open("answer.txt", "w")
    t = 1
    for i in q:
        all = map(int, i.split(" "))
        dancing = Dancing(all[1], all[2], all[3:])
        dancing.tripletAll()
        print "Case #{0}: {1}".format(t, dancing.bests())
        result.write("Case #{0}: {1}".format(t, dancing.bests())) if t == 1 else result.write("\nCase #{0}: {1}".format(t, dancing.bests()))
        print dancing.triplets
        print "suprise: {0}, given: {1}".format(dancing.countSuprise(), dancing.suprise)
        t += 1
    result.close()
    q.close()
    print "result successfully written to: answer.txt"

main()
