# -*- coding: utf8 -*-

import re

class Recycle(object):
    def __init__(self):
        pass

    def setRange(self, x):
        self.fro = x[0]
        self.to = x[1]
        self.pattern =  "[{0}-{1}].*[{0}-{1}].".format(str(self.fro)[0], str(self.to-self.fro)[0])
        self.regex =  re.compile(self.pattern)

    def getMaxPair(self):
        count = 0
        for i in range(self.fro, self.to):
            if self.regex.match(str(i)) and len(str(i)) !=  str(i).count(str(i)[0]):
                #print self.pattern, i
                count += 1
        return count



def main():
    print "Ensure you are in the question folder and the folder is writable"
    recycle =  Recycle()
    que = raw_input("enter the filename of the question: ")
    q = open(que, "r")
    q.readline() #omit the first line
    result = open("answer.txt", "w")
    t = 1
    for i in q:
        recycle.setRange(map(int, i.split(" ")))
        print "Case #{0}: {1}".format(t, recycle.getMaxPair())
        result.write("Case #{0}: {1}\n".format(t, recycle.getMaxPair()))
        t += 1
    result.close()
    q.close()
    print "result successfully written to: answer.txt"

main()
