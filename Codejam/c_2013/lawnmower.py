#Still experiencing some issues with the possibility check.
#Would try and fix this ASAP. Might be looking to a totally new method entirely!

SUCCESS = 0
PROCEED = 1
FAIL = 2

class Lawnmower(object):
    def __init__(self, lst):
        self.width = len(lst[0])
        self.length = len(lst)
        self.lst = lst

    def getVertical(self, index):
        return [row[index] for row in self.lst]

    def getHorizontal(self, index):
        return self.lst[index]

    def isSingleton(self):
        x = map(lambda l: l.count(1), self.lst)
        if (x.count(x[0])) == len(x):
            return True
        else:
            return False

    def traverseVertical(self):
        l = self.getHorizontal(0)
        if (l.count(2) == self.width):
            return PROCEED
        for i in xrange(self.width):
            if self.lst[0][i] == 1:
                lst = self.getVertical(i)
                if self.length != lst.count(1):
                    return FAIL
            else:
                if (self.lst[0][i] != self.lst[self.length - 1][i]):
                    return FAIL
        else:
            return SUCCESS

    def traverseHorizontal(self):
        l = self.getVertical(0)
        if (l.count(2) == self.length):
            return PROCEED
        for i in xrange(self.length):
            if (self.lst[i][0] == 1):
                lst = self.getHorizontal(i)
                if (self.width != lst.count(1)):
                    return FAIL
            else:
                if (self.lst[i][0] != self.lst[i][self.width - 1]):
                    return FAIL
        else:
            return SUCCESS


    def isPatternPossible(self):
        if (self.width == 1 or self.length == 1):
            return True
        if (self.isSingleton()):
            return True
        h = self.traverseHorizontal()
        if h == SUCCESS:
            return True
        else:
            v = self.traverseVertical()
            if v == SUCCESS:
                return True
            else:
                return False


def main(filename):
    q = open(filename, "r")
    cases = int(q.readline().strip())
    result = open("lawnmower_answer.txt", "w")
    for i in xrange(cases):
        n, m = map(int, q.readline().strip().split())
        lst = []
        for j in xrange(n):
            lst.append(map(int, q.readline().strip().split()))
        ans = "YES" if Lawnmower(lst).isPatternPossible() else "NO"
        print "Case #{0}: {1}".format(i + 1, ans)
        result.write("Case #{0}: {1}\n".format(i + 1, ans))
    result.close()
    q.close()

import cProfile
cProfile.run("main('lawnmower.txt')")