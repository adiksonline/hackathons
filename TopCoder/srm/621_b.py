class PairGameEasy:
    def able(self, a, b, c, d):
        check = [a, b]
        if (a + b) % 2 != (c + d) % 2:
            return "Not able to generate"
        if a > c or b > d or a == b == c == d:
            return "Not able to generate"
        while True:
            big = max(c, d)
            small = min(c, d)
            for i in check:
                if i in (c, d):
                    check.remove(i)
                    break
            if not check:
                return "Able to generate"
            c, d = small, big % small
            if not c or not d:
                return "Not able to generate"


p = PairGameEasy()
print p.able(100, 100, 100, 100)