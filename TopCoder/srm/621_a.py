class CandidatesSelectionEasy:
    def cmp(self, x, y):
        return cmp(x[self.n], y[self.n])

    def sort(self, score, x):
        score = list(score)
        self.n = x
        sort = sorted(score, self.cmp)
        lst = []
        for i in sort:
            index = score.index(i)
            lst.append(index)
            score[index] = "-"
        return tuple(lst)


c = CandidatesSelectionEasy()
print c.sort(("A", "C", "B", "C", "A"), 0)