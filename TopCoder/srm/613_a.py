__author__ = 'adiksonline'

from collections import Counter


class TaroString:
    def getAnswer(self, S):
        counter = Counter(S)
        for i in counter:
            if i not in ("A", "C", "T"):
                S = S.replace(i, "")
        return "Possible" if S == "CAT" else "Impossible"


taroString = TaroString()
for i in ("XCYAZTX", "CTA", "ACBBAT", "SGHDJHFIOPUFUHCHIOJBHAUINUIT", "CCCATT"):
    print taroString.getAnswer(i)