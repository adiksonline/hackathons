__author__ = 'adiksonline'


class SquareDetector:
    def __init__(self, square):
        self.square = square
        self.px, self.py = self.configure()

    def configure(self):
        x, y = -1, -1
        while x < 0 and y+1 < len(self.square):
            y += 1
            x = self.square[y].find("#")
        return x, y

    def confirmSquare(self):
        if self.px < 0 or self.py < 0:
            return False
        dimension = min(len(self.square)-self.py, len(self.square[0])-self.px)
        total = 0
        for snip in self.square:
            total += snip.count("#")
        if total != dimension**2:
            return False

        i = 1
        while i < dimension:
            if not self.verify(i):
                return False
            i += 1
        return True

    def verify(self, position):
        for i in xrange(self.px, self.px+position):
            if self.square[self.py][i] != "#":
                return False
        for i in xrange(self.py, self.py+position):
            if self.square[i][self.px] != "#":
                return False
        return True


def main(filename):
    q = open(filename)
    out = open(filename.rsplit(".")[0]+"_answer"+".txt", "w")
    number = int(q.readline())
    for i in xrange(number):
        length = int(q.readline())
        square = []
        for j in xrange(length):
            square.append(q.readline().strip())
        detector = SquareDetector(square)
        result = "Case #{0}: {1}".format(i+1, "YES" if detector.confirmSquare() else "NO")
        out.write("{}\n".format(result))
        print result
    q.close()
    out.close()

main("square_detector.txt")