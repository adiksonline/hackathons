__author__ = 'adiksonline'

from collections import  deque


class Player:
    def __init__(self, data):
        data = data.split(" ")
        self.name = data[0]
        self.shot = data[1]
        self.height = data[2]

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name

    def __unicode__(self):
        return self.name


def m_cmp(a, b):
    if a.shot > b.shot:
        return -1
    elif b.shot > a.shot:
        return 1
    else:
        return cmp(b.height, a.height)


class Management:
    def __init__(self, players, time, count):
        players = map(Player, players)
        players.sort(cmp=m_cmp)
        print players
        self.time = time
        self.count = count
        self.valid = deque(players[:count*2])
        self.waiting = deque(players[count*2:])

    def getValid(self):
        for i in xrange(self.time):
            temp = []
            temp.append(self.valid.pop())
            temp.append(self.valid.pop())
            self.waiting.extend(reversed(temp))
            temp = []
            temp.append(self.waiting.popleft())
            temp.append(self.waiting.popleft())
            self.valid.extendleft(reversed(temp))
        result = []
        for i in self.valid:
            result.append(i.name)
        return " ".join(result)


def main(filename):
    q = open(filename)
    out = open(filename.rsplit(".")[0]+"_answer"+".txt", "w")
    number = int(q.readline())
    for i in xrange(number):
        n, m, p = map(int, q.readline().strip().split())
        players = []
        for j in xrange(n):
            players.append(q.readline().strip())
        management = Management(players, m, p)
        print "Case #{0}: {1}".format(i+1, management.getValid())

main("basketball_game_example_input.txt")