#! /usr/bin python
# language: python

# Sourcecode for alphabet soup

def highestCount(text):
    """
    computes the number of times HACKERCUP shows up.
    """
    analyse = []
    for i in "HACKERCUP":
        analyse.append(text.count(i)/2 if (i == "C") else text.count(i))
    analyse.sort()
    return analyse[0]

def main():
    filename = raw_input("enter the path to the file: ")
    q = open(filename, "r")
    q.readline() #omit the first line
    result = open("answer.txt", "w")
    t = 1
    for i in q:
        print "Case #{0}: {1}\n".format(t, highestCount(i))
        result.write("Case #{0}: {1}\n".format(t, highestCount(i)))
        t += 1
    result.close()
    q.close()
    print "result successfully written to: answer.txt"

main()
