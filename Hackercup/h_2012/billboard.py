class BillBoard(object):
    def __init__(self, width, height, text):
        self.width = width
        self.height = height
        self.textData = text.split(" ")

    def getFontSize(self):
        b = 1
        while b:
            line = b
            occupied = 0
            test = 0
            u = 0
            for i in range(len(self.textData)):
                i = i-1 if u else i
                if len(self.textData[i]) * b > self.width:
                    test = 1
                    break
                elif line > self.height:
                    test = 1
                    break
                elif (occupied == 0) and ((len(self.textData[i]) * b) <= self.width):
                    occupied = len(self.textData[i]) * b
                elif (occupied != 0) and (((len(self.textData[i]) + 1) * b) + occupied) <= self.width:
                    occupied += (len(self.textData[i]) + 1) * b
                else:
                    line += b
                    occupied = 0
                    u = 1
                    #i -= 1
            if test: break
            b += 1
        return b-1
            



def main():
    filename = raw_input("enter the path to the file: ")
    q = open(filename, "r")
    q.readline() #omit the first line
    result = open("answer.txt", "w")
    t = 1
    for line in q:
        splitted = line.split(" ")
        board = [int(x) for x in splitted[:2]]
        text = " ".join(splitted[2:])
        myBoard = BillBoard(board[0], board[1], text)
        print "Case #{0}: {1}".format(t, myBoard.getFontSize())
        result.write("Case #{0}: {1}\n".format(t, myBoard.getFontSize()))
        t += 1
    result.close()
    q.close()
    print "result successfully written to: answer.txt"

main()
