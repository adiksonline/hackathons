#! /usr/bin python

class Auction:
    def __init__(self, (N, P, W, M, K, A, B, C, D)):
        self.number = N
        self.data = []
        self.data.append((P, W))
        for i in range(1, self.number):
            temp1 = ((A * self.data[i - 1][0] + B) % M) + 1
            temp2 = ((C * self.data[i - 1][1] + D) % K) + 1
            self.data.append((temp1, temp2))
            
    def getData(self):
        tb = 0
        tt = 0
        for i in self.data:
            b = 0
            t = 0
            for j in self.data:
                if (i[0] < j[0] and i[1] <= j[1]) or (i[1] < j[1] and i[0] <= j[0]):
                    b += 1
                if (i[0] > j[0] and i[1] >= j[1]) or (i[1] > j[1] and i[0] >= j[0]):
                    t += 1
            if b >= len(self.data) - 1:
                tb += 1
            if t >= len(self.data) - 1:
                tt += 1
        return "{0} {1}".format(tt, tb)


def main():
    filename = raw_input("enter the path to the file: ")
    q = open(filename, "r")
    q.readline() #omit the first line
    result = open("answer.txt", "w")
    t = 1
    for i in q:
        splitted = i.split(" ")
        args = [int(x) for x in splitted]
        myAuction = Auction(args)
        print "Case #{0}: {1}".format(t, myAuction.getData())
        result.write("Case #{0}: {1}\n".format(t, myAuction.getData()))
        t += 1
    result.close()
    q.close()
    print "result successfully written to: answer.txt"

main()
