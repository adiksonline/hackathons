#################################
lst = []

class CardGame:
    def __init__(self):
        self.stack = []
        self.summation = 0

    def doGame(self, k):
        self.sumMaxSubset(len(lst), k)
        return self.summation % 1000000007

    def sumMaxSubset(self, n, k):
        for i in xrange(n):
            if (n - i < k or k == 0):
                return
            self.stack.append(lst[len(lst) - n + i])
            if (k == 1):
                self.summation += max(self.stack)
                self.stack.pop()
                continue;
            else:
                self.sumMaxSubset(n - i - 1, k - 1);
                self.stack.pop()

def main(filename):
    global lst
    q = open(filename, "r")
    count = int(q.readline().strip())
    result = open("card_game_answer.txt", "w")
    for i in xrange(1, count + 1):
        n, k = map(int, q.readline().split())
        lst = map(long, q.readline().split())
        cardGame = CardGame()
        res = cardGame.doGame(k)
        print "Case #{0}: {1}".format(i, res)
        result.write("Case #{0}: {1}\n".format(i, res))
    result.close()
    q.close()


main("card_game.txt")
#import cProfile
#cProfile.run("main('card_game_test.txt')")