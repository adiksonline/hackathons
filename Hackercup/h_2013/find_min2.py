
def getMin(n, k, a, b, c, r):
    # Generate the sequence first
    A = [a]
    rep = k
    for i in xrange(1, k):
        A.append( (b * A[i - 1] + c) % r )
        if (A[-1] == A[0]):
            rep = i
            break
        #print next_input,

    for j in xrange(k, k + n % (k + 1) + 1):
    #for j in xrange(k, n):
        start = (j - k) % rep
        end = start + k
        s = set(A[start : end])
        if (end > len(A)):
            s.update(set(A[: end - len(A)]))
        m = max(s)
        for x in xrange(m):
            if x not in s:
                A.append(x)
                break;
        else:
            A.append(m + 1)

    return A[-1]


def main(filename):
    q = open(filename, "r"); result = open("find_min_answer.txt", "w")
    count = int(q.readline().strip())
    for i in xrange(1, count + 1):
        l = [q.readline()]
        l.append(q.readline().strip())
        n, k, a, b, c, r = map(long, "".join(l).split())
        res = getMin(n, k, a, b, c, r)
        print "Case #{0}: {1}".format(i, res)
        result.write("Case #{0}: {1}\n".format(i, res))
    q.close(); result.close()


#import cProfile
#cProfile.run("main('find_min_test.txt')")

main('find_the_min.txt')