def PERM = /([a-z]* *(:\))*(:\()*:*)*/; ACCEPT = /($PERM(\($PERM\))*$PERM)*/
def f = new File("INPUT_FILE"); scanner = new Scanner(f)
def out = new BufferedWriter(new FileWriter(new File("OUTPUT_FILE")))
int n = Integer.parseInt(scanner.nextLine().trim())
for (int a = 1; a <= n; a++)
    out.writeLine(scanner.nextLine().trim() ==~ ACCEPT ? "Case #$a: YES" : "Case #$a: NO")
    
out.flush(); out.close()