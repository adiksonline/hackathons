import string

def getBestBeauty(s):
    processed = s.translate(None, string.punctuation + string.whitespace).lower()
    def compare(x, y):
        a, b = processed.count(x), processed.count(y)
        if (a > b):
            return 1
        elif a == b:
            return 0
        else:
            return -1


    result = 0
    x = 26
    s = set(processed)
    l = list(s)
    l.sort(cmp = compare, reverse=True)
    for i in l:
        result += x * processed.count(i)
        x -= 1
    return result


def main(filename):
    q = open(filename, "r")
    q.readline() #omit the first line
    result = open("beautiful_strings_answer.txt", "w")
    t = 1
    for i in q:
        res = getBestBeauty(i)
        print "Case #{0}: {1}".format(t, res)
        result.write("Case #{0}: {1}\n".format(t, res))
        t += 1
    result.close()
    q.close()

main("beautiful_strings.txt")