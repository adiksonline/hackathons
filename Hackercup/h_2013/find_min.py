from collections import deque

def getMin(n, k, a, b, c, r):
    # Generate the sequence first
    A = [a] # A - a list
    #A = deque(A)
    rep = 0
    for i in xrange(1, k):
        A.append( (b * A[i - 1] + c) % r )
        if (A[-1] == A[0]):
            rep = i

    for j in xrange(k, n):
        s = sorted(set(A[j - k : j]))  #set - no repeating member
        if (s[0] > 0):
            A.append(0)
        else:
            #DARK MAGIC STARTS HERE
            upper = len(s) - 1
            while (upper < (s[upper] - s[0])):
                upper-= 1
                if (upper == 0):
                    A.append(s[0] + 1)
                    break
            else:
                A.append(s[upper] + 1)
            #DARK MAGIC ENDS HERE

    #print A
    return A[-1] #return the last item in the list A[n - 1]


def main(filename):
    q = open(filename, "r")
    count = int(q.readline().strip()) #omit the first line
    result = open("find_min_answer.txt", "w")
    for i in xrange(1, count + 1):
        s = q.readline()
        s += q.readline().strip()
        n, k, a, b, c, r = map(int, s.split())
        res = getMin(n, k, a, b, c, r)
        print "Case #{0}: {1}".format(i, res)
        result.write("Case #{0}: {1}\n".format(i, res))
    result.close()
    q.close()


import cProfile
cProfile.run("main('find_min_test.txt')")