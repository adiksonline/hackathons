import re

accept = "([a-z]* *(:\))*(:\()*:*)*"
exp = re.compile("^({0}(\({0}\))*{0})*$".format(accept))

def main(filename):
    q = open(filename, "r")
    q.readline() #omit the first line
    result = open("balanced_smileys_answer.txt", "w")
    t = 1
    for i in q:
        res = "YES" if exp.match(i) else "NO"
        print "Case #{0}: {1}".format(t, res)
        result.write("Case #{0}: {1}\n".format(t, res))
        t += 1
    result.close()
    q.close()

main("balanced_smileys.txt")