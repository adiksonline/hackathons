# Codechef august challenge: Hello hello

def getBestPlan(default, addons):
    plansOffset = []
    for addon in addons:
        offset = (default[0] * default[1] * addon[0]) - (addon[2] + addon[1] * default[1] * addon[0])
        plansOffset.append(offset)
    result = 0
    for i in plansOffset:
        if i > result:
            result = i
    return result if (result == 0) else plansOffset.index(result) + 1


def main():
    cases = input()
    for i in xrange(cases):
        temp = raw_input().split()
        default = float(temp[0]), int(temp[1]), int(temp[2]) # rate, time, addons
        addons = [] # months, rate, cost
        for j in xrange(int(default[2])):
            temp = raw_input().split()
            addon = int(temp[0]), float(temp[1]), int(temp[2])
            addons.append(addon)
        print getBestPlan(default, addons)


main()