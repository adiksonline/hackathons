# Codechef August challenge: Little elephant and lemonade

def getTotalBottles(visits, bottleInRooms):
    ans = 0
    for i in visits:
        bottles = bottleInRooms[i]
        if bottles:
            ans += bottles.pop()
    return ans


def main():
    cases = input()
    for i in range(cases):
        n, m = map(int, raw_input().split())
        visits = map(int, raw_input().split())
        bottleInRooms = []
        for i in range(n):
            bottleInRooms.append(sorted(map(int, raw_input().split())[1:]))
        print getTotalBottles(visits, bottleInRooms)


main()