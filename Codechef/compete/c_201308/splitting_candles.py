# Codechef August Challenge - Splitting candies

def getResult(total, students):
    return total / students, total % students

def main():
    cases = input()
    for i in range(cases):
        total, students = map(int, raw_input().split())
        if (students <= 0):
            print "0 {}".format(total)
        else:
            print "{0[0]} {0[1]}".format(getResult(total, students))


main()