__author__ = 'adiksonline'

n = 2014
values = [True] * n
for i in xrange(3,int(n**0.5)+1,2):
    if values[i]:
        values[i*i::2*i]=[False]*((n-i*i-1)/(2*i)+1)


def minForPrime(x, y):
    current = x+y
    i = current + (current % 2) + 1
    while not values[i]:
        i += 2
    return i - current


def main():
    cases = input()
    for i in xrange(cases):
        x, y = map(int, raw_input().split())
        print minForPrime(x, y)

main()