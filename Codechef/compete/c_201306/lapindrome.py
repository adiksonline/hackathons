

def testLapindrome(string):
    l = list(string)
    midl = len(l) / 2
    midr = midl + 1 if len(l) % 2 else midl
    #print sorted(l[ : midl]), sorted(l[midr : ])
    if sorted(l[ : midl]) == sorted(l[midr : ]):
        return "YES"
    return "NO"



def main(filename):
    q = open(filename, "r")
    q.readline() #omit the first line
    result = open("res/lapindrome_a.txt", "w")
    for i in q:
        res = testLapindrome(i.strip())
        print res
        result.write("{0}\n".format(res))
    result.close()
    q.close()

main("res/lapindrome_q.txt")