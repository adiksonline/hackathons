
def transform(string):
    stack = []
    ans = []
    for i in string:
        if i == "(":
            stack.append(i)
        elif i == ")":
            if stack[-1] != "(":
                ans.append(stack.pop())
            stack.pop()
        elif i in ("+", "-", "*", "/", "^", "%"):
            stack.append(i)
        else:
            ans.append(i)
            if stack and stack[-1] != "(":
                ans.append(stack.pop())
    return "".join(ans)

def main():
    count = input()
    for i in xrange(count):
        print transform(raw_input())


main()