
def getMaxSum(triangle):
    for i in xrange(len(triangle) - 2, -1, -1):
        for j in xrange(len(triangle[i])):
            if triangle[i + 1][j] > triangle[i + 1][j + 1]:
                triangle[i][j] += triangle[i + 1][j]
            else:
                triangle[i][j] += triangle[i + 1][j + 1]
    return triangle[0][0]


def main():
    count = input()
    for i in xrange(count):
        size = input()
        triangle = []
        for j in xrange(size):
            triangle.append(map(int, raw_input().split()))
        print getMaxSum(triangle)


main()