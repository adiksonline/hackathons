
def countZeros(number):
    ans = 0
    while number >= 5:
        number = number / 5
        ans += number
    return ans

def main():
    count = input()
    l = []
    for i in xrange(count):
        l.append(input())
    for i in l:
        print countZeros(i)


main()