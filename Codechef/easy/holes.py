
def countHoles(string):
    holes = 0
    for i in ("A", "D", "O", "R", "P", "Q"):
        holes += string.count(i)
    holes += 2 * string.count("B")
    return holes

def main():
    n = input()
    for i in xrange(n):
        print countHoles(raw_input())

main()