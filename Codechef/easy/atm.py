# -*- coding: utf8 -*-

def getBalance(amt, ibal):
    if amt > ibal - 0.5 or amt % 5 != 0:
        return ibal
    return ibal - amt - 0.5

def main():
    n = raw_input()
    n = map(float, n.split())
    print "%0.2f" %getBalance(n[0],n[1])

main()
