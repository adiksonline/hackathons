# -*- coding: utf8 -*-

import sys
from PyQt4.QtCore import *
from PyQt4.QtGui import *

"""
This is the module that creates the basic graphical user interface display."""

class BasicUi(QMainWindow):
    """
    This is the basic user interface class."""
    def __init__(self):
        """
        The basic user interface constructor."""
        super(BasicUi, self).__init__()
        self.initUi()
        self.setWindowTitle("My ATM")

    def initUi(self):
        """
        This method sets up the basic user interface."""
        self.myCentralWidget = QWidget()
        self.screenView = QTextBrowser()
        self.buttons = []
        self.labels = []
        lLayout = QGridLayout()
        rLayout = QGridLayout()
        for i in xrange(8):
            temp = QPushButton("<>")
            tlabel = QLabel()
            self.buttons.append(temp)
            self.labels.append(tlabel)
            j = i % 4
            lLayout.addWidget(temp, j, 0) if i < 4 else rLayout.addWidget(temp, j, 1)
            lLayout.addWidget(tlabel, j, 1) if i < 4 else rLayout.addWidget(tlabel, j, 0)
        spacer = QSpacerItem(50, 20, QSizePolicy.Expanding, QSizePolicy.Expanding)
        bLayout = QHBoxLayout()
        bLayout.addLayout(lLayout)
        bLayout.addWidget(self.screenView)
        #bLayout.addSpacerItem(spacer)
        bLayout.addLayout(rLayout)

        layout = QVBoxLayout()
        #layout.addWidget(self.screenView)
        layout.addLayout(bLayout)
        self.myCentralWidget.setLayout(layout)
        self.setCentralWidget(self.myCentralWidget)


if __name__ == "__main__":
    app = QApplication(sys.argv)
    basic = BasicUi()
    basic.show()
    app.exec_()
