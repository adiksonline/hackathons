# -*- coding: utf8 -*-

class AtmException(Exception):
    def __init__(self, msg = ""):
        super(AtmException, self).__init__(msg)
