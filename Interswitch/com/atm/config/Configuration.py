# -*- coding: utf8 -*-

import codecs
import os
from xml.etree.cElementTree import ElementTree

class Configuration(object):
    def __init__(self):
        tree = ElementTree()
        config = tree.parse("data/config.xml")
        self.homeMenus = list(config.getiterator("MENU"))
