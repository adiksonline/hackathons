# -*- coding: utf8 -*-

from com.atm.ui.basicUi import *
from com.atm.config.Configuration import Configuration

class ATM(BasicUi):
    def __init__(self):
        super(ATM, self).__init__()
        self.config = Configuration()
        self.currentItem = None
        self.applyCss()
        self.level = 0
        self.updateMenu(self.config.homeMenus, self.level)
        self.setupConnections()
        self.screenView.setHtml("<font size=+5 color=green>Welcome to our Automated Teller Machine! Cash Dispenser<br>\
        Select your choice from the menus below</font>")

    def applyCss(self):
        s = open("resources/mycss.css").read()
        self.setStyleSheet(s)

    def updateMenu(self, array, level):
        for i in self.labels:
            i.setText("")
        j = 0
        for i in array:
            self.labels[j].setText(i.attrib["name"])
            j += 1

    def setupConnections(self):
        for i in self.buttons:
            i.clicked.connect(self.updateUi)

    def performAction(self, action, id):
        if action == "back":
            self.level = 0
            self.updateMenu(self.config.homeMenus, self.level)
        elif action == "input":
            text = QInputDialog.getText(self, "Input", self.currentItem[id].attrib["msg"])[0]
            self.screenView.setText(self.screenView.toPlainText() + ". You entered " + text)
        elif action == "display":
            text = self.currentItem[id].attrib["msg"]
            QMessageBox.information(self, "Display", self.currentItem[id].attrib["msg"])
        else:
            self.screenView.setHtml("<font size=+2 color=green>This is just a test pane! You just clicked {0}</font>".format(self.labels[id].text()))

    def updateUi(self):
        senderId = self.buttons.index(self.sender())
        if self.level == 1:
            self.performAction(self.currentItem[senderId].attrib["action"], senderId)
        else:
            if senderId == 7:
                self.close()
            else:
                self.currentItem = self.config.homeMenus[senderId]
                self.updateMenu(self.currentItem, self.level)
                self.screenView.setText("<font size=+2 color=green>{0}</font>".format(self.config.homeMenus[senderId].attrib["msg"]))
                self.level = 1
