# -*- coding: utf8 -*-

import sys
from atm import *

def main():
    QApplication.setStyle(QStyleFactory.create("Cleanlooks"))
    app = QApplication(sys.argv)
    myAtm = ATM()
    myAtm.showFullScreen()
    app.exec_()

main()
