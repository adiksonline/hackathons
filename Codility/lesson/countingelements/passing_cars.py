def solution(A):
    # write your code in Python 2.6
    res = 0
    max_ = 0
    for i in xrange(1, len(A)):
        cur = A[i]
        if cur:
            x = i - 1
            while x >= 0 and not A[x]:
                max_ += 1
                x -= 1
            res += max_
    return res if res <= 1000000000 else -1
