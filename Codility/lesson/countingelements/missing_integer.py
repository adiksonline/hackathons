__author__ = 'adiksonline'


def solution(A):
    temp = {k: True for k in A if k > 0}
    index = -1
    A = temp.keys()
    # The assumption here is that A is gonna be almost sorted since
    # temp is a hash map and the keys are already numbers.
    A.sort()
    for index, i in enumerate(A):
        if index+1 != i:
            return index+1
    return index+2

print solution([-3, 2, -1, 1, 2, 3, -1, 4, 6, 2, 3] + range(10000) + [-1, 1, 2, 3] * 1000)