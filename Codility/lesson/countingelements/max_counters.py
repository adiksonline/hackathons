def solution(N, A):
    # write your code in Python 2.6
    counter = {}
    max_ = 0
    high = max_
    for k, x in enumerate(A):
        if x == N + 1:
            max_ = high
            counter.clear()
        elif 1 <= x <= N:
            key = x - 1
            if key in counter:
                counter[key] += 1
            else:
                counter[key] = max_ + 1
            high = max(high, counter[key])
    res = [max_] * N
    for i in counter:
        res[i] = counter[i]
    return res
