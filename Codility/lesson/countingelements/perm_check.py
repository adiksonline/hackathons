def solution(A):
    # write your code in Python 2.6
    return 1 if sum(xrange(1, len(A) + 1)) == sum(set(A)) else 0
