def solution(X, A):
    # write your code in Python 2.6
    temp = set()
    for i, k in enumerate(A):
        temp.add(k)
        if len(temp) == X:
            return i
    return -1
