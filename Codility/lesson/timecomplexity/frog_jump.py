def solution(X, Y, D):
    # write your code in Python 2.6
    Y -= X
    if Y % D:
        return Y // D + 1
    else:
        return Y // D
    pass
