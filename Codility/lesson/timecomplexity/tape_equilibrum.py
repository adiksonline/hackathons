def solution(A):
    sum_ = sum(A)
    number = 0
    min_ = None
    for n in xrange(len(A) - 1):
        number += A[n]
        diff = abs(number * 2 - sum_)
        if min_ is None or diff < min_:
            min_ = diff
    return min_
