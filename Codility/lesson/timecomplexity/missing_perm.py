def solution(A):
    # write your code in Python 2.6
    return sum(xrange(1, len(A) + 2)) - sum(A)
