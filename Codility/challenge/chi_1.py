def merge(A):
    la = len(A)
    if (la < 2):
        return 0
    else:
        A.sort()
        summ = (A[0] + A[1]) * (la - 1)
        for x in xrange(2, la):
            summ += A[x] * (la - x)
        return summ


def test():
    #for i in range(1000):
    print merge([100, 250, 1000])
    print merge([100, 1000, 250])
    print merge([250, 1000, 100])


import profile

profile.run("test()")