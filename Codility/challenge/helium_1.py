def solution(A):
    ans = 0
    valid = True
    l = len(A)
    big = l
    if l == 0: return -1
    if l == 1: return 0
    index = 1
    while index < l:
        print big, index, ans
        diff = A[ans] - A[index]
        if diff == 0 and big > index:
            ans = index
            valid = True
        elif diff > 0:
            valid = False
        elif diff < 0:
            big = index
            slc = A[index + 1:]
            if slc and min(slc) >= A[index]:
                ans = index
                valid = True
        index += 1
    return ans if valid else -1


import cProfile

cProfile.run("print solution( [4, 2, 2, 3, 3, 6, 7, 8, 9, 1])")