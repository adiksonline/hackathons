from collections import deque


class Graph:
    def __init__(self, N):
        self.vertices = dict((v, []) for v in xrange(N))

    def addEdge(self, src, dst, weight):

        a = self.vertices[src]
        lo = 0;
        hi = len(a)

        while lo < hi:
            mid = (lo + hi) // 2
            if weight < a[mid][0]:
                hi = mid
            else:
                lo = mid + 1
        a.insert(lo, (weight, dst))

    def getUnvisited(self, v, w):
        a = self.vertices[v]
        lo = 0;
        hi = len(a)

        while lo < hi:
            mid = (lo + hi) // 2
            if w < a[mid][0]:
                hi = mid
            else:
                lo = mid + 1

        if lo >= len(a):
            return None, None
        else:
            return a.pop(lo)


    def setup(self, A, B, C):
        self.roads = len(A)
        for src, dst, weight in zip(A, B, C):
            self.addEdge(src, dst, weight)
            if src != dst:
                self.addEdge(dst, src, weight)

    def getBestTraversal(self):
        best = 0
        stack = deque()
        for vertex in self.vertices:
            stack.append((vertex, 0, 0))
            while stack:
                node, weight, weight_ = stack[-1]
                w, unvisited = self.getUnvisited(node, weight)
                if unvisited is None:
                    l = len(stack) - 1
                    if l >= self.roads:
                        return l
                    if l > best:
                        best = l
                    v, w, w_ = stack.pop()
                    if stack:
                        vc, wc, wc_ = stack.pop()
                        wc = w_
                        self.addEdge(vc, v, w_)
                        stack.append((vc, wc, wc_))
                else:
                    stack.append((unvisited, w, w))
                #print [(v, w_) for v, w, w_ in stack]

        return best


def solution(N, A, B, C):
    graph = Graph(N)
    graph.setup(A, B, C)
    return graph.getBestTraversal()


print solution(6, [0, 1, 1, 2, 3, 4, 5], [1, 2, 3, 3, 4, 5, 0], [4, 3, 2, 5, 6, 6, 8])
print solution(2, [0, 0, 1, 0, 1], [0, 0, 0, 1, 1], [6, 1, 4, 2, 5])
print solution(5, [2, 1, 3, 0, 1, 3, 0], [1, 1, 4, 4, 0, 2, 1], [4, 15, 10, 7, 2, 5, 16])
print solution(5, [1, 4, 3, 2], [4, 0, 1, 1], [5, 7, 20, 3])