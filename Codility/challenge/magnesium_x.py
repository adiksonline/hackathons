class Vertex:
    def __init__(self, label):
        self.label = label
        self.next = []

    def addNext(self, vertex, weight):

        lo = 0;
        hi = len(self.next)
        a = self.next

        while lo < hi:
            mid = (lo + hi) // 2
            if weight < a[mid][0]:
                hi = mid
            else:
                lo = mid + 1
        a.insert(lo, (weight, vertex))

    def getUnvisited(self, w):
        #print "Vertex {}. weight = {}".format(self.label, w)
        #print "Next {} = {}".format(self.label, [(v.label, w_) for w_, v in self.next])

        a = self.next
        lo = 0;
        hi = len(a)

        while lo < hi:
            mid = (lo + hi) // 2
            if w < a[mid][0]:
                hi = mid
            else:
                lo = mid + 1

        if lo >= len(a):
            return None, 0
        else:
            weight, vertex = a[lo]
            a.remove((weight, vertex))
            return vertex, weight


class Graph:
    def __init__(self, N):
        self.vertices = [Vertex(i) for i in xrange(N)]

    def setup(self, A, B, C):
        for src, dst, weight in zip(A, B, C):
            self.vertices[src].addNext(self.vertices[dst], weight)
            if src != dst:
                self.vertices[dst].addNext(self.vertices[src], weight)

    def getBestTraversal(self):
        best = 0
        stack = []
        for vertex in self.vertices:
            stack += [(vertex, 0, 0)]
            while stack:
                node, weight, weight_ = stack[-1]
                unvisited, w = node.getUnvisited(weight)
                if unvisited:
                    stack += [(unvisited, w, w)]
                #print [(v.label, w_) for v, w, w_ in stack]
                else:
                    l = len(stack) - 1
                    if l > best:
                        best = l
                    v, w, w_ = stack.pop()
                    if stack:
                        vc, wc, wc_ = stack.pop()
                        wc = w_
                        vc.addNext(v, w_)
                        stack += [(vc, wc, wc_)]
        return best


def solution(N, A, B, C):
    # write your code in Python 2.6
    graph = Graph(N)
    graph.setup(A, B, C)
    return graph.getBestTraversal()


def main():
    for i in xrange(1000):
        solution(2, [0, 0, 1, 0, 1], [0, 0, 0, 1, 1], [6, 1, 4, 2, 5])


print solution(6, [0, 1, 1, 2, 3, 4, 5], [1, 2, 3, 3, 4, 5, 0], [4, 3, 2, 5, 6, 6, 8])
print solution(2, [0, 0, 1, 0, 1], [0, 0, 0, 1, 1], [6, 1, 4, 2, 5])
print solution(5, [2, 1, 3, 0, 1, 3, 0], [1, 1, 4, 4, 0, 2, 1], [4, 15, 10, 7, 2, 5, 16])
print solution(5, [1, 4, 3, 2], [4, 0, 1, 1], [5, 7, 20, 3])
# import cProfile
# cProfile.run("main()")