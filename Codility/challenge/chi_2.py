import cProfile
import time


def complementary_pairs(K, A):
    if len(A) < 1:
        return 0
    result = 0
    frequencyDict = {}
    for i in A:
        if frequencyDict.has_key(i):
            frequencyDict[i] = frequencyDict[i] + 1
        else:
            frequencyDict[i] = 1
    keysList = sorted(frequencyDict.keys())
    lo, hi = 0, len(keysList) - 1
    while lo < hi:
        summ = keysList[lo] + keysList[hi]
        if summ == K:
            result += 2 * frequencyDict[keysList[lo]] * frequencyDict[keysList[hi]]
            hi -= 1
        elif summ > K:
            hi -= 1
        else:
            lo += 1
    if (2 * keysList[lo] == K):
        result += frequencyDict[keysList[lo]] ** 2
    return result


lst = [3] * 50000
lst = [1, 8, -3, 0, 1, 3, -2, 4, 5]
cProfile.run("print complementary_pairs(6, lst)")
t1 = time.time()
complementary_pairs(6, lst)
t2 = time.time()
print "Time used:", t2 - t1