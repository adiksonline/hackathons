import cProfile


def getOnes(lst):
    summ = 0
    for i in lst:
        summ += (2 << i)
    return bin(summ * 3).count("1")


print cProfile.run("getOnes([444417, 1234567, 9999999])")