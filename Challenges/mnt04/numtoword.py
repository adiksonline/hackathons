
mapping =  {0 : "", 1 : "thousand", 2 : "million",
 3 : "billion", 4 : "trillion", 5 : "quadrillion"}

word = {0 : "", 1 : "one", 2 : "two", 3 : "three", 4 : "four", 5 : "five", 6 : "six", 7 : "seven", 8 : "eight", 9 : "nine"}
tens =  {1 : "ten", 2 : "twenty", 3 : "thirty", 4 : "forty", 5 : "fifty", 6 : "sixty", 7 : "seventy", 8 : "eighty", 9 : "ninety", 0 : "",}
corner = {"11" : "eleven", "12": "twelve", "13" : "thirteen", "14" : "fourteen", "15" : "fifteen", "16" : "sixteen", "17" : "seventeen",
          "18" : "eighteen", "19" : "nineteen"}

def convert(n):
    if n in ("11", "12", "13", "14", "15", "16", "17", "18", "19"):
        return corner[n]
    lst = list(str(n))
    if len(lst) > 3: raise ValueError("max convertible is 3 digits")
    ans = ""
    count =  len(lst) -  1
    for i in lst:
        if count == 1 and lst[-2] != "0":
            tmp = "".join(lst[-2 : ])
            if tmp in corner.keys():
                ans += "and {0}".format(corner[tmp])
                return ans
            elif len(lst) <= 2 or (len(lst) > 2 and lst[-3] == "0"):
                ans += "{0}".format(tens[int(i)])
            else:
                ans += "and {0}".format(tens[int(i)])
        elif count == 2 and lst[-3] != "0":
            ans += "{0} hundred ".format(word[int(i)])
        elif count == 0 and lst[-1] != "0":
            if len(lst) == 0:
                ans += "{0} ".format(word[int(i)])
            elif len(lst) > 1 and lst[-2] != "0":
                ans += "-{0} ".format(word[int(i)])
            elif len(lst) > 1 and lst[-2] == "0":
                ans += "and {0} ".format(word[int(i)])
            else:
                ans += "{0} ".format(word[int(i)])
        count -= 1
    return ans.strip()

def toWord(n):
    ans =  ""
    a = []
    s = str(n)
    i = 0
    tmp =  ""
    for j in range(len(s)-1,-1, -1):
        tmp = s[j] + tmp
        if len(tmp) == 3:
            a.insert(0, tmp)
            i = 0
            tmp = ""
    rem = len(s) % 3
    if rem > 0:
        a.insert(0, s[: rem])
    count =  len(a) -  1
    for i in a:
        if convert(i):
            t = "{0} {1}".format(convert(i), mapping[count])
            if count != 0 and t.startswith("and"):
                t = t[4 :]
            ans += t
            if count > 0 and count < len(a):
                ans +=  ", "
        count -= 1
    if ans.endswith(", "):
        ans = ans[:-2]
    return ans

def main():
    n = int(raw_input("enter the number: "))
    print toWord(n)

main()
