import re

myMap = {2 : "[abcABC2]", 3 : "[defDEF3]", 4 : "[ghiGHI4]", 5 : "[jklJKL5]", 6 : "[mnoMNO6]", 7 : "[pqrsPQRS7]", 8 : "[tuvTUV8]", 9 : "[wxyzWXYZ9]", 0 : " 0", 1 : "[-_,.;()1]"}

matchMap = {}

def getMatchList(entryList, searchString):
    ansList = []
    pattern = re.compile(generatePattern(searchString))
    for i in entryList:
        mObject = re.search(pattern, i)
        if mObject:
            matchMap[i] = mObject.group()
            ansList.append(i)
    ansList.sort(cmp = compare)
    return "\n".join(ansList) if len(ansList) else "NOT FOUND"

def generatePattern(st):
    patternList = [myMap[int(i)] for i in st]
    return "".join(patternList)


def compare(a, b):
    for i in a.split(":"):
        aMatch = i.find(matchMap[a])
        if aMatch >= 0:
            break
    for j in b.split(":"):
        bMatch = j.find(matchMap[b])
        if bMatch >= 0:
            break
    if aMatch > bMatch:
        return 1
    elif aMatch < bMatch:
        return -1
    else:
        return 0

def inputTest():
    lst = []
    count = input()
    for i in xrange(count):
        lst.append(raw_input())
    searchString = raw_input()
    print getMatchList(lst, searchString)

inputTest()