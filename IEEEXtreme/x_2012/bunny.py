class Bunny:
    def __init__(self, count):
        self.adult = count
        self.young = 0
        self.baby = 0
        self.time = 0

    def inc(self, time):
        self.time += time
        initAdult = self.adult
        if (self.time % 30 == 0):
            remainingAdults = initAdult * 0.75 * 0.7
            #self.adult = (initAdult + self.young) * 0.75 * 0.7
        else:
            remainingAdults = initAdult
        if (self.time == 15):
            self.young = 0
        else:
            self.young = 0.9 * initAdult
        self.baby = 0.9 * remainingAdults
        if self.time % 30 == 0:
            self.adult = remainingAdults + self.young
        else:
            self.adult = remainingAdults + (self.young * 0.75 * 0.7)

    def getRemainder(self, year):
        iterCount = 1 * 12 * 2
        for i in xrange(iterCount):
            self.inc(15)
            print "Stage:", i, "Time:", self.time
            print "Adult:", self.adult
            print "Young:", self.young
            print "Babies:", self.baby
        return int(self.adult + self.young + self.baby)


bunny = Bunny(444)
print bunny.getRemainder(1)