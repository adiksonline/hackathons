
players = ("Alice", "Bob")

def returnWinner(a, b, winner):
    # A player with an even number will always take advantage of it to win the other player!!! :)

    return (False if (not (a % 2) or not (b % 2)) else True)

def main():
    x = input("Enter size of test case: ")
    for j in xrange(x):
        i = raw_input("Enter n1 and n2: ")
        a, b = i.split(" ")
        print ("{0}\n".format(players[returnWinner(int(a), int(b), False)]))

main()