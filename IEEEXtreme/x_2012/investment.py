import bisect

class BobsAccount:
    def __init__(self, years, initCapital, salary, annualBonus, monthTax, salaryRate, taxRate, houseList):
        self.serviceYears = years
        self.capital = initCapital
        self.salary = salary
        self.annualBonus = annualBonus
        self.monthlyTaxRate = monthTax / 100# on the house
        self.usableSalaryRate = salaryRate /100 # on salary
        self.oneTimeTaxRate = taxRate / 100 # on the house
        self.houseList = houseList
        self.currentHouse = []
        self.year = 1
        self.month = 1

    def invest(self):
        self.buyHouse(getTimeHash(self.year, self.month))
        for month in xrange(12 * self.serviceYears):
            self.updateMonthCapital()
            #print "Month: ", month + 1, ": ", self.capital
        worth = self.capital
        for i in self.currentHouse:
            worth += self.getHouseCost(i, self.getClosestTime(i, getTimeHash(self.year, self.month)))
        return worth


    def getHouseValue(self, index, timeHash):
        timeHash = self.getClosestTime(index, timeHash)
        return self.houseList[index][timeHash][0] - self.houseList[index][timeHash][1]

    def getHouseCost(self, index, timeHash):
        timeHash = self.getClosestTime(index, timeHash)
        return self.houseList[index][timeHash][1]

    def getHouseRent(self, houseIndex, timeHash):
        return self.houseList[houseIndex][timeHash][0]

    def getClosestTime(self, houseIndex, tim):
        timeLst = sorted(self.houseList[houseIndex].keys())
        if tim in timeLst:
            return tim
        else:
            return timeLst[bisect.bisect(timeLst, tim) - 1]

    def getBestChoice(self, timeHash, houseList):
        house, value = 0, self.getHouseValue(0, timeHash)
        for i in xrange(len(self.houseList)):
            timeHash = self.getClosestTime(i, timeHash)
            if i not in houseList and self.houseList[i][timeHash][1] < self.capital and self.getHouseValue(i, timeHash) > value:
                house, value = i, self.getHouseValue(i, timeHash)
        return house # assumming that there is always a house to buy.

    def getWorstChoice(self, timeHash):
        if len(self.currentHouse) < 2:
            return -1
        else:
            house, value = 0, self.getHouseValue(0, timeHash)
        for houseIndex in self.currentHouse:
            if self.getHouseValue(houseIndex, timeHash) < value:
                house, value = houseIndex, self.getHouseValue(houseIndex, timeHash)
        return house


    def buyHouse(self, timeHash):
        houseIndex = self.getBestChoice(timeHash, self.currentHouse)
        houseCost = self.getHouseCost(houseIndex, self.getClosestTime(houseIndex, timeHash))
        while len(self.currentHouse) < 15 and self.capital > houseCost:
            #print "House bought"
            self.capital -= houseCost
            self.capital -= (self.oneTimeTaxRate * houseCost)
            self.capital -= (self.monthlyTaxRate * houseCost)
            self.currentHouse.append(houseIndex)
            houseIndex = self.getBestChoice(timeHash, self.currentHouse)
            houseCost = self.getHouseCost(houseIndex, timeHash)

    def sellHouse(self, houseIndex, timeHash):
        self.currentHouse.remove(houseIndex)
        self.capital += self.getHouseCost(houseIndex, timeHash)

    def updateMonthCapital(self):
        if self.month < 12:
            self.month += 1
        else:
            self.year += 1
            self.month = 1
        if self.month == 12:
            self.capital += self.annualBonus
        self.capital += self.usableSalaryRate * self.salary
        if self.currentHouse:
            timeHash = self.getClosestTime(self.currentHouse[0], getTimeHash(self.year, self.month))
            self.buyHouse(timeHash)
            for i in self.currentHouse:
                self.capital += self.getHouseRent(i, self.getClosestTime(i, getTimeHash(self.year, self.month)))
                self.capital -= self.getHouseCost(i, self.getClosestTime(i, getTimeHash(self.year, self.month))) * self.monthlyTaxRate

            worstCaseHouse = self.getWorstChoice(timeHash)
            if worstCaseHouse >= 0:
                #print "Worst case house:", worstCaseHouse
                if self.getHouseValue(worstCaseHouse, timeHash) < self.getHouseValue(self.getBestChoice(timeHash, self.houseList), timeHash):
                    self.sellHouse(self.getWorstChoice(timeHash), timeHash)
                    self.buyHouse(timeHash)





def getTimeHash(year, month):
    return year * 12 + month

def parseHouse(houseData):
    house = {}
    houseD = houseData.split(" ")
    lst = map(float, houseD)
    i = 0
    while i < len(lst):
        house[getTimeHash(lst[i], lst[i + 1])] = [lst[i + 2], lst[i + 3]]
        i += 4
    return house

def inputTest():
    years = input()
    initCapital = input()
    usableIncomeRate = input()
    salary, annualBonus = map(float, raw_input().strip().split(" "))
    monthTax = input()
    oneTimeTax = input()
    houseList = []
    for house in xrange(input()):
        houseList.append(parseHouse(raw_input().strip()))

    bobsAccount = BobsAccount(years, initCapital, salary, annualBonus,
                             monthTax,
                             usableIncomeRate,
                             oneTimeTax, houseList)
    print "%0.2f" %(bobsAccount.invest())


inputTest()