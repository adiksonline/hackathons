
def getVoucher(a, b, c, d):
    if b > 10000 or c > 10000 or d > 10000:
        return "ERROR"
    if a > 1000000:
        return "ERROR"
    if (b + c) == a:
        return "0 0 0 {0}".format(d)
    elif (c + d) == a:
        return "0 {0} 0 0".format(b)
    elif (b + d) == a:
        return "0 0 {0} 0".format(c)
    elif (b - a) > 0:
        return "0 {0} {1} {2}".format(b-a, c, d)
    elif (c - (b - a)) > 0:
        return "0 0 {0} {1}".format(c - (b -a), d)
    elif (d - (c - (b - a))) > 0:
        return "0 0 0 {0}".format(d - (c - (b - a)))
    else:
        return "ERROR"


def inputTest():
    try:
        myList = map(int, raw_input().strip().split(" "))
        if len(myList) != 4:
            print "ERROR"
        else:
            a, b, c, d = myList
            print getVoucher(a, b, c, d)
    except:
        print "ERROR"


inputTest()