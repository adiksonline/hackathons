
def getMinMoves(tankA, tankB, volume):
    if volume < tankA or volume < tankB:
        d = abs(tankB - tankA)
        if volume % d:
            return "no"
        else:
            return 3 * (volume / d)
    else:
        e1 = int(volume / tankA)
        ans1 = e1 * 2
        f1 = volume % tankA
        d1 = abs(tankB - tankA)
        if f1 % d1:
            return "no"
        else:
            ans_a = ans1 + 3 * (f1 / d1)
        e2 = int(volume / tankB)
        ans2 = e2 * 2
        f2 = volume % tankB
        d2 = abs(tankA - tankB)
        if f2 % d2:
            return "no"
        else:
            ans_b = ans2 + 3 * (f2 / d2)
        return min(ans_a, ans_b)


def inputTest():
    try:
        myMap = map(int, raw_input().strip().split(" "))
        if len(myMap) != 3:
            print "no"
        else:
            a, b, c = myMap
        print getMinMoves(a, b, c)
    except:
        print "no"

inputTest()