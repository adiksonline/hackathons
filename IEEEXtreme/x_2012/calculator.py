
class Calculator:
    def __init__(self, size, style, op1, op2, oper, space):
        self.size = size
        self.style = style
        self.op1 = op1
        self.op2 = op2
        self.operator = oper
        self.spaceCount = space
        evaluate = eval(op1 + oper + op2)
        self.answer = str(evaluate)

    def getDisplay(self, char):
        if (char == '1'):
            return self.one()
        elif (char == '2'):
            return self.two()
        elif (char == '3'):
            return self.three()
        elif (char == '4'):
            return self.four()
        elif (char == '5'):
            return self.five()
        elif (char == '6'):
            return self.six()
        elif (char == '7'):
            return self.seven()
        elif (char == '8'):
            return self.eight()
        elif (char == '9'):
            return self.nine()
        elif (char == '0'):
            return self.zero()
        elif (char == '-'):
            return self.neg()

    def getOperator(self, char):
        if (char == '+'):
            return self.plus()
        elif (char == '-'):
            return self.sub()
        elif (char == '*'):
            return self.mul()
        elif (char == '/'):
            return self.div()
        elif (char == '%'):
            return self.mod()

    def printIt(self, lst):
        i = 0
        while i  <  self.size:
            display = []
            j = 0
            while j < len(lst):
                display.append(''.join(lst[j][i]))
                j += 1
            i += 1
            spaces = ' '*self.spaceCount
            print spaces.join(display)

    def display(self):
        maxSize = 1
        maxSize += max([len(self.answer), len(self.op1), len(self.op2)])

        lst = []
        i = 0
        while i < maxSize - len(self.op1):
            lst.append(self.createList())
            i += 1
        for e in self.op1:
            lst.append(self.getDisplay(e))
        self.printIt(lst)
        line = (maxSize *self.size) + (self.spaceCount * (maxSize - 1))
        print ' '*line

        lst = []
        lst.append(self.getOperator(self.operator))
        i = 1
        while i < maxSize - len(self.op2):
            lst.append(self.createList())
            i += 1
        for e in self.op2:
            lst.append(self.getDisplay(e))
        self.printIt(lst)
        print ' '*line
        print '-'*line
        print ' '*line

        lst = []
        i = 0
        while i < maxSize - len(self.answer):
            lst.append(self.createList())
            i += 1
        for e in self.answer:
            lst.append(self.getDisplay(e))
        self.printIt(lst)

    def createList(self):
        lst = [' '] *self.size
        k = 0
        while k < self.size:
            lst[k] = [' '] *self.size
            k += 1
        return lst

    def one(self):
        lst = self.createList()
        mid = (self.size/2)
        i = 0
        while i < self.size:

            if i < self.size-1:

                lst[i][mid] = self.style
                angle = mid - i
                if (angle > 0):
                    lst[i][angle] = self.style
            else:
                j = 0
                while j < self.size:
                    lst[i][j] = self.style
                    j += 1
            i += 1
        return lst

    def two(self):
        lst = self.createList()
        mid = (self.size/2)
        i = 0
        while i < self.size:
            if (i == 0) or (i == self.size-1) or (i == mid):
                j = 0
                while j < self.size:
                    lst[i][j] = self.style
                    j += 1
            else:
                if (i < mid):
                    lst[i][self.size - 1] = self.style
                elif (i > mid):
                    lst[i][0] = self.style
            i += 1
        return lst

    def three(self):
        lst = self.createList()
        mid = (self.size/2)
        i = 0
        while i < self.size:
            if (i == 0) or (i == self.size-1) or (i == mid):
                j = 0
                while j < self.size:
                    lst[i][j] = self.style
                    j += 1
            else:
                if (i < mid):
                    lst[i][self.size - 1] = self.style
                elif (i > mid):
                    lst[i][self.size - 1] = self.style
            i += 1
        return lst

    def four(self):
        lst = self.createList()
        mid = (self.size/2)
        i = 0
        while i < self.size:
            if (i < mid):
                lst[i][0] = self.style
                lst[i][self.size - 1] = self.style
            elif (i == mid):
                j = 0
                while j < self.size:
                    lst[i][j] = self.style
                    j += 1
            else:
                lst[i][self.size - 1] = self.style
            i += 1
        return lst

    def five(self):
        lst = self.createList()
        mid = (self.size/2)
        i = 0
        while i < self.size:
            if (i == 0) or (i == self.size-1) or (i == mid):
                j = 0
                while j < self.size:
                    lst[i][j] = self.style
                    j += 1
            else:
                if (i < mid):
                    lst[i][0] = self.style
                elif (i > mid):
                    lst[i][self.size - 1] = self.style
            i += 1
        return lst

    def six(self):
        lst = self.createList()
        mid = (self.size/2)
        i = 0
        while i < self.size:
            if (i == 0) or (i == self.size-1) or (i == mid):
                j = 0
                while j < self.size:
                    lst[i][j] = self.style
                    j += 1
            else:
                if (i < mid):
                    lst[i][0] = self.style
                elif (i > mid):
                    lst[i][self.size - 1] = self.style
                    lst[i][0] = self.style
            i += 1
        return lst

    def seven(self):
        lst = self.createList()
        i = 0
        while i < self.size:
            if (i == 0):
                j = 0
                while j < self.size:
                    lst[i][j] = self.style
                    j += 1
            else:
                lst[i][self.size - 1 - i] = self.style
            i += 1
        return lst

    def eight(self):
        lst = self.createList()
        mid = (self.size/2)
        i = 0
        while i < self.size:
            if (i == 0) or (i == self.size-1) or (i == mid):
                j = 0
                while j < self.size:
                    lst[i][j] = self.style
                    j += 1
            else:
                lst[i][0] = self.style
                lst[i][self.size - 1] = self.style
            i += 1
        return lst

    def nine(self):
        lst = self.createList()
        mid = (self.size/2)
        i = 0
        while i < self.size:
            if (i == 0) or (i == self.size-1) or (i == mid):
                j = 0
                while j < self.size:
                    lst[i][j] = self.style
                    j += 1
            else:
                if (i < mid):
                    lst[i][0] = self.style
                    lst[i][self.size - 1] = self.style
                elif (i > mid):
                    lst[i][self.size - 1] = self.style
            i += 1
        return lst

    def zero(self):
        lst = self.createList()
        i = 0
        while i < self.size:
            if (i == 0) or (i == self.size-1):
                j = 0
                while j < self.size:
                    lst[i][j] = self.style
                    j += 1
            else:
                lst[i][0] = self.style
                lst[i][self.size - 1] = self.style
            i += 1
        return lst

    def neg(self):
        lst = self.createList()
        mid = (self.size/2)
        j = 0
        while j < self.size:
            lst[mid][j] = self.style
            j += 1
        return lst

    def plus(self):
        lst = self.createList()
        mid = (self.size/2)
        i = 0
        while i < self.size:
            if (i != 0 and i != self.size - 1):
                if (i == mid):
                    j = 0
                    while j < self.size:
                        lst[i][j] = '+'
                        j += 1
                else:
                    lst[i][mid] = '+'
            i += 1
        return lst

    def sub(self):
        lst = self.createList()
        mid = (self.size/2)
        j = 0
        while j < self.size:
            lst[mid][j] = '-'
            j += 1
        return lst

    def mul(self):
        lst = self.createList()
        j = 0
        while j < self.size:
            if (j != 0 and j != self.size - 1):
                i = 0
                while i < self.size:
                    if (i != 0 and i != self.size - 1):
                        lst[j][i] = '*'
                    i += 1
            j += 1
        return lst

    def div(self):
        lst = self.createList()
        j = 0
        while j < self.size:
            lst[j][self.size - 1 - j] = '/'
            j += 1
        return lst

    def mod(self):
        lst = self.createList()
        j = 0
        while j < self.size:
            if (j == 0) or (j == self.size - 1):
                lst[j][j] = '%'
            lst[j][self.size - 1 - j] = '%'
            j += 1
        return lst


operand1 = raw_input()
operand2 = raw_input()
operator = raw_input()
font = raw_input()
size = input()
space = input()
calculator = Calculator(size, font, operand1, operand2, operator, space)
calculator.display()