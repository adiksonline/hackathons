
def getCoinValues(price, deposit):
    coins = [100, 25, 10, 5]
    if (price % 5) or price > deposit:
        return "ERROR"
    else:
        price = deposit - price
        lst = [0] * 4
        while price > 0:
            for i in xrange(len(coins)):
                if coins[i] <= price:
                    lst[i] = price // coins[i]
                    price %= coins[i]
        return " ".join(map(str, lst))

def inputTest():
    try:
        data = map(int, raw_input().strip().split(" "))
        if len(data) != 2:
            print "ERROR"
        else:
            price, deposit = data
            print getCoinValues(price, deposit)
    except:
        print "ERROR"

inputTest()