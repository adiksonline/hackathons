import sys

def getValidVatSum(lst):
    summ = 0
    for vat, amount in lst:
        vat = str(vat)
        if len(vat) == 8:
            vat = "0" + vat
        if len(vat) == 9:
            S = long(vat[-1]) * 0 + long(vat[-2]) * 2 + long(vat[-3]) * 4 + long(vat[-4]) * 8 + long(vat[-5]) * 16 + long(vat[-6]) * 32 + long(vat[-7]) * 64 + long(vat[-8]) * 128 + long(vat[-9]) * 256
            Y = S % 11
            if (Y == 10 and vat[-1] == "0") or Y == long(vat[-1]):
                summ += amount
    return summ



def inputTest():
    lst = []
    console = sys.stdin
    inp = console.readline().strip()
    while inp:
        lst.append(map(long, inp.split(" ")))
        inp  = console.readline().strip()
    print getValidVatSum(lst)

inputTest()