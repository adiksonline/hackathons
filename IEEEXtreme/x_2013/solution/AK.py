__author__ = 'adiksonline'


class Node:
    def __init__(self, label):
        self.label = label
        self.children = []
        self.parents = []


class Graph:
    def __init__(self, n):
        self.nodes = [Node(i+1) for i in xrange(n)]
        self.root = None

    def addRelation(self, i, j):
        i -= 1; j-= 1
        if not self.root:
            self.root = self.nodes[i]
        self.nodes[i].children.append(self.nodes[j])
        self.nodes[j].parents.append(self.nodes[i])

def main():
    n = input()
    graph = Graph(n)
    ans = []
    for i in range(n-1):
        i, j = map(int, raw_input().strip().split())
        graph.addRelation(i, j)
    node = graph.root
    while True:
        if len(node.children) == 1:
            node = node.children[0]
        else:
            break
    ans.append(node.label)
    while True:
        if len(node.parents) == 1:
            node = node.parents[0]
            ans.append(node.label)
        else:
            break
    for i in sorted(ans):
        print i

main()