__author__ = 'adiksonline'
import sys

cols = [[0]*1000 for i in xrange(1000)]

def output(a, lIndex, mIndex):
    global cols
    if lIndex < 0:
        return
    output(a, lIndex -1, cols[lIndex][mIndex])
    sys.stdout.write("[{0},{1}]".format(lIndex, mIndex))

def process(arr):
    global cols
    l = len(arr); mRisk = sys.maxint; mIndex = -1
    for i in xrange(1, l):
        nLen = len(arr[0])
        for j in xrange(nLen):
            minn = sys.maxint
            mIndex = -1
            st = max(0, j-1)
            end = min(len(arr[0]) - 1, j+1)
            for k in xrange(st, end+1):
                if arr[i-1][k] < minn:
                    minn = arr[i-1][k]
                    mIndex = k
            cols[i][j] = mIndex
            arr[i][j] += minn
    for i in xrange(len(arr[0])):
        if arr[-1][i] < mRisk:
            mIndex = i
            mRisk = arr[-1][i]
    sys.stdout.write("Minimum risk path = ")
    output(arr, len(arr)-1, mIndex)
    print ""
    print "Risks along the path = {}".format(mRisk)

def main():
    c, r = map(int, raw_input().split())
    a = []
    for i in xrange(c):
        a.append(map(int, raw_input().split()))
    process(a)

main()
