__author__ = 'adiksonline'

class Decipher:
    def __init__(self, froList, toList):
        self.map = {}
        self.__decipher(froList, toList)

    def __decipher(self, froList, toList):

        # Word length algorithm
        oF = [len(i) for i in froList]
        oT = [len(i) for i in toList]

        froF = froList[:]
        toF = toList[:]
        for i in froList:
            for j in toList:
                if len(i) == len(j) and oF.count(len(i)) == 1:
                    self.map.update(dict(zip(i, j)))
                    froF.append(i)
                    toF.remove(j)
                    froF.remove(i)
                    break
        # Ends here


        # Assisted Mapping
        tempFroF = froF[:]
        tempToF = toF[:]
        for i in froF:
            pre = self.__precode(i)
            for j in toF:
                if len(i) == len(j) and self.__score(pre, j):
                    self.map.update(dict(zip(i, j)))
                    tempFroF.remove(i)
                    tempToF.remove(j)
                    break


    def __precode(self, key):
        ans = ""
        for i in key:
            ans += self.map.get(i, "*")
        return ans

    def __score(self, k, v):
        sc = 0
        total = len(k.replace("*", ""))
        for i in xrange(len(k)):
            if k[i] != "*" and k[i] == v[i]:
                sc += 1
        return True if sc == total else False


    def decode(self, froList):
        ans = []
        for i in froList:
            ans.append("".join(self.map.get(j, "*") for j in i))
        return " ".join(ans)



def main():
    count = input()
    toList = []
    for i in xrange(count):
        toList.append(raw_input().strip())
    raw_input()
    froList = raw_input().strip().split()
    decipher = Decipher(froList, toList)
    print decipher.decode(froList).upper()

main()