__author__ = 'adiksonline'

class AL:

    def __init__(self, string):
        self.string = list(string)
        self.strLen = len(self.string)
        self.count = len(string)
        self.stack = []

    def calculate(self):
        for i in xrange(2, self.strLen + 1):
            self.getCombi(self.strLen, i)
        return self.count % 12345678

    def getCombi(self, n, r):
        for i in xrange(n):
            if n - i < r or r == 0:
                return;
            self.stack.append(self.string[self.strLen - n + i])
            if r == 1:
                if self.isPalindrome(self.stack):
                    self.count += 1
                self.stack.pop()
                continue
            else:
                self.getCombi(n -i - 1, r - 1)
                self.stack.pop()


    def isPalindrome(self, string):
        return string == list(reversed(string))


al = AL("XXXXXXXXXXXXXXXXXXXX")
#al = AL("dcec")
import cProfile
cProfile.run("print(al.calculate())")