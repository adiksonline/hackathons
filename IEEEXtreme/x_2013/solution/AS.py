__author__ = 'adiksonline'

class Edge:
    def __init__(self, i, j = None):
        if j:
            self.src = i
            self.dest = j
        else:
            self.src, self.dest = i.split()

def hasNode(nodes, node):
    for n in nodes:
        if n.label == node.label:
            return n
    return node

def main():
    dest = raw_input().strip()
    nodes = []
    line = raw_input().strip()
    while line:
        if line == "A A":
            break
        a, b = line.split()
        node1 = Node(a)
        node2 = Node(b)

        nodeA = hasNode(nodes, node1)
        nodeB = hasNode(nodes, node2)
        nodes.append(node1)
        nodes.append(node2)

        nodeA.relatives.append(nodeB)
        nodeB.relatives.append(nodeA)
        line = raw_input().strip()

    if nodes:
        paths = 0
        stack = []
        shortestPaths = None
        shortest = 0
        stack.append(hasNode(nodes, Node("F")))
        while stack:
            currentNode = stack.pop()
            currentNode.visited.append(currentNode.label)
            #print currentNode.label
            if currentNode.label == dest:
                #print "Gotten"
                if shortest == 0:
                    shortestPaths = currentNode
                    shortest = len(currentNode.visited)
                if len(currentNode.visited) == shortest:
                    for i in xrange(shortest):
                        if shortestPaths.visited[i] > currentNode.visited[i]:
                            shortestPaths = currentNode
                if len(currentNode.visited) < shortest:
                    shortest = len(currentNode.visited)
                    shortestPaths = currentNode
                paths += 1
                continue
            for relative in currentNode.relatives:
                if not currentNode.hasVisited(relative):
                    temp = Node(relative.label)
                    temp.relatives = relative.relatives[:]
                    for visits in currentNode.visited:
                        temp.visited.append(visits)
                    stack.append(temp)
        if paths:
            print "Total Routes: {}".format(paths)
            print "Shortest Route Length: {}".format(shortest)
            print "Shortest Route after Sorting of Routes of length {}:".format(shortest),
            for visits in shortestPaths.visited:
                print visits,
            print ""
        else:
            print "No Route Available from F to {}".format(dest)


class Node:
    def __init__(self, label):
        self.label = label
        self.relatives = []
        self.visited = []

    def hasVisited(self, relative):
        for visits in self.visited:
            if relative.label == visits:
                return True
        return False

    def __hash__(self):
        return self.label

main()